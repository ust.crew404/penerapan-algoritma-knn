<?php 

class M_data extends CI_Model{
	function tampil_data(){
		return $this->db->get('data_training');
	}

	function tampil($table){
		return $this->db->get($table);
	}

	function set_data($where,$table){
	return $this->db->get_where($table,$where);
	}
	function urutkan($limit)
	{
		$this->db->order_by('distance', 'ASC');
		 $this->db->limit($limit);
		$query = $this->db->get('temp');
		return $query;
	}
	function count($limit)
	{
		$this->db->order_by('distance', 'ASC');
		 $this->db->limit($limit);
		$query = $this->db->get('temp');
		return $query;
	}
	public function Insert($table,$data){
        $res = $this->db->insert($table, $data); // Kode ini digunakan untuk memasukan record baru kedalam sebuah tabel
        return $res; // Kode ini digunakan untuk mengembalikan hasil $res
    }
}