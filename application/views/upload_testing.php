	<!-- Main -->
				<section class="wrapper style1">
					<div class="container">
						<div id="content">

							<!-- Content -->

								<article>
									<h2>Upload Data Testing</h2>
									<form action="<?php echo base_url('awal/data_aksi_testing');?>" method="post" enctype="multipart/form-data">
								    <input type="file" name="file"/>
								    <input type="submit" value="Upload file"/>
								    <p><span><font color="red">NP: File berupa data Excel ^_^ </font></span></p>
								</form>
								<p>Atau</p>
								 <input type="submit" id="myBtn" value="Input Data Testing" class="btn btn-success"/><hr>
								</article>
								<div id="myModal" class="modal">

                  <!-- Modal content -->
                  <div class="modal-content">
                    <span class="close">&times;</span>
                     <form action="<?php echo base_url('/awal/data_testing_aksi'); ?>" method="post">
                        <div class="form-group">
                          <label >Nama :</label>
                          <input type="text" class="form-control"  name="nama" placeholder="Nama" required>
                        </div>
                        <div class="form-group">
                          <label >Semester 1 :</label>
                          <input type="text" class="form-control"  name="smt1" placeholder="Nilai Semester 1" required>
                        </div>
                        <div class="form-group">
                          <label >Semester 2 :</label>
                          <input type="text" class="form-control"  name="smt2" placeholder="Semester 2" required>
                        </div>
                        <div class="form-group">
                          <label >Semester 3 :</label>
                          <input type="text" class="form-control"  name="smt3" placeholder="Semester 3" required>
                        </div>
                        <hr>
                         <input type="submit" value="Simpan" class="btn btn-success"/>
                      </form>
                  </div>
            </div>
						</div>
					</div>
				</section>
				<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
