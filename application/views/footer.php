
		
			<!-- Footer -->
				<div id="footer">
					<!-- Icons -->
						<ul class="icons">
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-github"><span class="label">GitHub</span></a></li>
							<li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
							<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
						</ul>

					<!-- Copyright -->
						<div class="copyright">
							<ul class="menu">
								<li>&copy; Copyright by</li><li>Kithil <a href="#">-Dev</a></li>
							</ul>
						</div>

				</div>

		</div>

		<!-- Scripts -->
			<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
			<script src="<?php echo base_url('assets/js/jquery.dropotron.min.js');?>"></script>
			<script src="<?php echo base_url('assets/js/skel.min.js');?>"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<?php echo base_url('assets/js/main.js');?>"></script>

	</body>
</html>