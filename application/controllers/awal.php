<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Awal extends CI_Controller {

	function __construct()
	{
				parent::__construct();		
				$this->load->model('m_data');
                $this->load->helper('url');
                $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
	}
	public function index()
	{
		$this->load->view('header');
		$this->load->view('content');
		$this->load->view('footer');
	}
	public function create()
	{
		$this->load->view('header');
		$this->load->view('upload');
		$this->load->view('footer');
	}
	public function create_testing()
		{
			$this->load->view('header');
			$this->load->view('upload_testing');
			$this->load->view('footer');
		}
	public function list(){
		$data['data'] = $this->m_data->tampil_data()->result();
		$this->load->view('header', $data);
		$this->load->view('lihat_data', $data);
		$this->load->view('footer', $data);
	}
	 public function data_aksi()
    {
        $this->db->query('truncate table data_training');
        $fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv|xlsx';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->db->query('truncate table data_training');
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        $inputFileName = './assets/'.$media['file_name'];
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    "id"=> $rowData[0][0],
                    "nama"=> $rowData[0][1],
                    "smt1"=> $rowData[0][2],
                    "smt2"=> $rowData[0][3],
                    "smt3"=> $rowData[0][4],
                    "status"=> $rowData[0][5]
                );
                 
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("data_training",$data);
            //    delete_files($media['file_path']);
                     
            }
        redirect('awal/list');
        echo "Data berhasil di Upload!";
    } 
    public function data_aksi_testing()
    {
        $this->db->query('truncate table data_testing');
        $fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv|xlsx';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->db->query('truncate table data_training');
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        $inputFileName = './assets/'.$media['file_name'];
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    "id"=> $rowData[0][0],
                    "nama"=> $rowData[0][1],
                    "smt1"=> $rowData[0][2],
                    "smt2"=> $rowData[0][3],
                    "smt3"=> $rowData[0][4]
                );
                 
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("data_testing",$data);
            //    delete_files($media['file_path']);
                     
            }
        redirect('awal/proses');
        echo "Data berhasil di Upload!";
    }

    public function proses(){
        $data['training'] = $this->m_data->tampil('data_training')->result();
        $data['testing'] = $this->m_data->tampil('data_testing')->result();
        $this->db->query('truncate table temp');
        $this->load->view('header', $data);
        $this->load->view('proses', $data);
        $this->load->view('footer', $data);

    }
    public function hasil(){
        // $this->db->query('truncate table hasil_akhir');
        $this->db->query('truncate table temp_hasil');
        $limit = $this->input->post('k');
        $data['hasil'] = $this->m_data->urutkan($limit)->result();
        $data['testing'] = $this->m_data->tampil('data_testing')->result();
        $this->load->view('header', $data);
        $this->load->view('hasil', $data);
        $this->load->view('footer', $data);

    }
    public function data_testing_aksi(){
            $this->db->query('truncate table data_testing');
            $nama = $this->input->post('nama');
            $smt1 = $this->input->post('smt1');
            $smt2 = $this->input->post('smt2');
            $smt3 = $this->input->post('smt3');

            $data = array('nama' => $nama ,
                          'smt1' => $smt1,
                          'smt2' => $smt2,
                          'smt3' => $smt3);
            $this->m_data->Insert('data_testing',$data);
            redirect('awal/proses');

    }
    public function cetak(){
       // $data['cetak'] = $this->m_data->tampil('hasil')->result();
        $data['cetak'] = $this->m_data->tampil('hasil_akhir')->result();
        $this->load->view('cetak', $data);
    }
}
